package br.com.sistemabancario.caixaeletronico.saque;

import br.com.sistemabancario.caixaeletronico.notas.NenhumaNotaDisponivelException;
import br.com.sistemabancario.caixaeletronico.notas.NotasService;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;

@SpringBootTest
class SaqueServiceTest {

    @Mock
    private NotasService notasService;

    @InjectMocks
    private SaqueService saqueService;

    @Test
    void dadoQueTenhaApenasUmaNotaDeValorDezQuandoRealizarSaqueDeValorDezDeveEntregarUmaNotaDeDez() throws NenhumaNotaDisponivelException, BusinessException {
        // Given
        final ArrayList<NotaDto> notaEsperadas = Lists.newArrayList(getNotaDto(1, 10.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaEsperadas);

        // When
        final List<NotaDto> notaSaque = saqueService.realizar(10.0);

        assertEquals(notaEsperadas, notaSaque);
        verify(notasService).atualizarQuantidades(anyList());
    }

    @Test
    void dadoQueTenhaTodasAsNotasDisponiveisQuandoRealizarSaqueDeValor150DeveEntregarUmaNotaDeCemEUmaNotaDeCinquenta() throws NenhumaNotaDisponivelException, BusinessException {
        // Given
        final ArrayList<NotaDto> notaDisponiveis = Lists.newArrayList(getNotaDto(10, 100.0), getNotaDto(10, 50.0),
                getNotaDto(10, 20.0), getNotaDto(10, 10.0));
        final ArrayList<NotaDto> notaEsperadas = Lists.newArrayList(getNotaDto(1, 100.0), getNotaDto(1, 50.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaDisponiveis);

        // When
        final List<NotaDto> notaSaque = saqueService.realizar(150.0);

        // then
        assertThat(notaSaque, containsInAnyOrder(notaEsperadas.toArray()));
        verify(notasService).atualizarQuantidades(anyList());
    }

    @Test
    void dadoQueNaoTenhaNota100DisponivelQuandoRealizarSaqueDeValor210DeveEntregar4Notas50EUmaDeDez() throws NenhumaNotaDisponivelException, BusinessException {
        // Given
        final List<NotaDto> notaDisponiveis = Lists.newArrayList(getNotaDto(10, 50.0),
                getNotaDto(10, 20.0), getNotaDto(10, 10.0));
        final List<NotaDto> notaEsperadas = Lists.newArrayList(getNotaDto(4, 50.0), getNotaDto(1, 10.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaDisponiveis);

        // When
        final List<NotaDto> notaDoSaque = saqueService.realizar(210.0);

        // then
        assertThat(notaDoSaque, containsInAnyOrder(notaEsperadas.toArray()));
        verify(notasService).atualizarQuantidades(anyList());
    }

    @Test
    public void dadoQueTenha10Notas100ETodasAsOutrasNotasDisponivelQuandoRealizarSaqueDeValor1200DeveEntregar10Notas100E4NotasDe50() throws NenhumaNotaDisponivelException, BusinessException {
        // Given
        final List<NotaDto> notaDisponiveis = Lists.newArrayList(getNotaDto(10, 100.0),
                getNotaDto(10, 50.0), getNotaDto(10, 20.0), getNotaDto(10, 10.0));
        final List<NotaDto> notaEsperadas = Lists.newArrayList(getNotaDto(10, 100.0), getNotaDto(4, 50.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaDisponiveis);

        // When
        final List<NotaDto> notaDoSaque = saqueService.realizar(1200.0);

        // then
        assertThat(notaDoSaque, containsInAnyOrder(notaEsperadas.toArray()));
        verify(notasService).atualizarQuantidades(anyList());
    }

    @DisplayName("Dado um valor de saque maior que o disponível" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem valor de saque acima do disponível" +
            "e status 404")
    @Test
    public void valorIndisponivelDeveLancarExcecao() throws NenhumaNotaDisponivelException {
        final List<NotaDto> notaDisponiveis = Lists.newArrayList(getNotaDto(10, 100.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaDisponiveis);

        ValorDeSaqueAcimaDoDisponivel thrown = assertThrows(
                ValorDeSaqueAcimaDoDisponivel.class,
                () -> saqueService.realizar(1200.0),
                "Expected saqueService.realizar(1200.0) to throw ValorDeSaqueAcimaDoDisponivel, mas não ocorreu"
        );

        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains("Valor acima do disponível."));
    }

    @DisplayName("Dado um valor de saque maior que o permitido" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem valor de saque acima do permitido" +
            "e status 404")
    @Test
    public void valorAcimaPermitidoDeveLancarExcecao() throws NenhumaNotaDisponivelException {
        final List<NotaDto> notaDisponiveis = Lists.newArrayList(getNotaDto(30, 100.0));
        Mockito.when(notasService.getNotasDisponiveis()).thenReturn(notaDisponiveis);

        ValorDeSaqueAcimaDoPermitido thrown = assertThrows(
                ValorDeSaqueAcimaDoPermitido.class,
                () -> saqueService.realizar(2500.0),
                "Expected saqueService.realizar(2500.0) to throw ValorDeSaqueAcimaDoPermitido, mas não ocorreu"
        );

        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains("Valor de saque acima do permitido."));
    }

    @DisplayName("Dado um valor de saque não mútiplo de dez" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem Valor não é múltiplo de dez" +
            "e status 404")
    @Test
    public void valorNaoMultiploDeDezDeveLancarExcecao() {
        ValorNaoEMultiploDeDez thrown = assertThrows(
                ValorNaoEMultiploDeDez.class,
                () -> saqueService.realizar(15.0),
                "Expected saqueService.realizar(15.0) to throw ValorNaoEMultiploDeDez, mas não ocorreu"
        );

        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains("Valor deve ser múltiplo de dez."));
    }

    @DisplayName("Dado um valor de saque menor que dez" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem Valor não é múltiplo de dez" +
            "e status 404")
    @Test
    public void valorMenorQueDezDeveLancarExcecao() {
        ValorNaoEMultiploDeDez thrown = assertThrows(
                ValorNaoEMultiploDeDez.class,
                () -> saqueService.realizar(0.0),
                "Expected saqueService.realizar(15.0) to throw ValorNaoEMultiploDeDez, mas não ocorreu"
        );

        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains("Valor deve ser múltiplo de dez."));
    }

    private NotaDto getNotaDto(int quantidade, double valor) {
        return NotaDto.builder().quantidade(quantidade).valor(valor).build();
    }
}