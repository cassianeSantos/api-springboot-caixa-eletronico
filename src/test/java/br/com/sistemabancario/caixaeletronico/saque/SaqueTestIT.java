package br.com.sistemabancario.caixaeletronico.saque;

import br.com.sistemabancario.caixaeletronico.CaixaEletronicoApplication;
import br.com.sistemabancario.caixaeletronico.notas.NotasRepository;
import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

@SpringBootTest(classes = CaixaEletronicoApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SaqueTestIT {

    @LocalServerPort
    private int port;

    private NotasRepository notasRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public SaqueTestIT(NotasRepository notasRepository, ObjectMapper objectMapper) {
        this.notasRepository = notasRepository;
        this.objectMapper = objectMapper;
    }

    @DisplayName("Dado que tenha todas as notas disponíveis após reposição com quantidade 10" +
            "quando realizar o saque de 200 reais" +
            "então deve retornar duas notas de cem" +
            "e deve conter oito notas de cem restantes.")
    @Test
    public void deveSacar200reais() throws JsonProcessingException {
        // given
        notasRepository.deleteAll();
        notasRepository.save(Nota.builder().valor(100.0).quantidade(10).build());
        notasRepository.save(Nota.builder().valor(50.0).quantidade(10).build());
        notasRepository.save(Nota.builder().valor(20.0).quantidade(10).build());
        notasRepository.save(Nota.builder().valor(10.0).quantidade(10).build());

        double valor = 200.0;
        List<NotaDto> notasEsperadas = Lists.newArrayList(NotaDto.builder().quantidade(2).valor(100.0).build());

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);

        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertEquals(HttpStatus.OK, r.getStatusCode());
        List<NotaDto> notasDoSaque = objectMapper.readValue(r.getBody(), new TypeReference<>() {
        });
        assertThat(notasDoSaque, containsInAnyOrder(notasEsperadas.toArray()));
        final Optional<Nota> byValor = notasRepository.findByValor(100.0);
        Assertions.assertTrue(byValor.isPresent());
        Assertions.assertEquals(8, byValor.get().getQuantidade());
    }

    @DisplayName("Dado que tenha apenas uma nota de 100" +
            "quando realizar o saque de 200 reais" +
            "então deve retornar um erro de ValorDeSaqueAcimaDoDisponível" +
            "e conter o status 400 Bad Request.")
    @Test
    public void deveRetorarValorAcimaDoDisponivel() {
        // given
        notasRepository.deleteAll();
        notasRepository.save(Nota.builder().valor(100.0).quantidade(1).build());
        double valor = 200.0;

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);
        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Valor acima do disponível"));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, r.getStatusCode());
    }

    @DisplayName("Dado que tenha não tenha nenhuma nota disponível" +
            "quando realizar o saque de 200 reais" +
            "então deve retornar um erro de NenhumaNotaDisponivel" +
            "e conter o status 404 Not Found.")
    @Test
    public void deveRetorarNenhumaNotaDisponivel() {
        // given
        notasRepository.deleteAll();
        double valor = 200.0;

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);
        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Nenhuma nota disponível."));
        Assertions.assertEquals(HttpStatus.NOT_FOUND, r.getStatusCode());
    }

    @DisplayName("Dado um valor de saque maior que o permitido" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem valor de saque acima do permitido" +
            "e status 404")
    @Test
    public void deveLancarExcecaoValorDeSaqueAcimaDoPermitido() {
        // given
        notasRepository.deleteAll();
        notasRepository.save(Nota.builder().valor(100.0).quantidade(30).build());
        double valor = 3000.0;

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);
        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Valor de saque acima do permitido."));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, r.getStatusCode());
    }

    @DisplayName("Dado um valor de saque não mútiplo de dez" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem Valor não é múltiplo de dez" +
            "e status 404")
    @Test
    public void valorNaoMultiploDeDezDeveLancarExcecao() {
        // given
        double valor = 15.0;

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);
        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Valor deve ser múltiplo de dez."));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, r.getStatusCode());
    }

    @DisplayName("Dado um valor de saque menor que dez" +
            "quando realizar o saque" +
            "então deve lançar um exceção com a mensagem Valor não é múltiplo de dez" +
            "e status 404")
    @Test
    public void valorMenorQueDezDeveLancarExcecao() {
        // given
        double valor = 5.0;

        // when
        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl() + valor, HttpMethod.GET, new HttpEntity<>(null, null), String.class);
        // then
        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Valor deve ser múltiplo de dez."));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, r.getStatusCode());
    }

    private String getUrl() {
        return "http://localhost:" + port + "/saque/";
    }
}
