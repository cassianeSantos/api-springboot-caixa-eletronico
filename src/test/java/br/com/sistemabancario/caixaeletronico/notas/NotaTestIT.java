package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.CaixaEletronicoApplication;
import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = CaixaEletronicoApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotaTestIT {

    @LocalServerPort
    private int port;

    private NotasService notasService;
    private NotasRepository notasRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public NotaTestIT(NotasService notasService, NotasRepository notasRepository, ObjectMapper objectMapper) {
        this.notasService = notasService;
        this.notasRepository = notasRepository;
        this.objectMapper = objectMapper;
    }

    @Test
    public void deveRetornarNotasDisponiveis() throws JsonProcessingException {
        notasRepository.deleteAll();
        final Nota nota = Nota.builder().valor(100.0).quantidade(1).build();
        notasRepository.save(nota);

        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl(), HttpMethod.GET, new HttpEntity<>(null, null), String.class);

        Assertions.assertNotNull(r.getBody());
        Assertions.assertEquals(HttpStatus.OK, r.getStatusCode());
        List<NotaDto> notasDoSaque = objectMapper.readValue(r.getBody(), new TypeReference<>() {});
        Assertions.assertEquals(1, notasDoSaque.size());
        Assertions.assertEquals(nota.getQuantidade(), notasDoSaque.get(0).getQuantidade());
        Assertions.assertEquals(nota.getValor(), notasDoSaque.get(0).getValor());
    }


    @Test
    public void deveRetornarErroPorNaoHaverNotasDisponiveis() {
        notasRepository.deleteAll();

        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl(), HttpMethod.GET, new HttpEntity<>(null, null), String.class);

        Assertions.assertNotNull(r.getBody());
        Assertions.assertTrue(r.getBody().contains("Nenhuma nota disponível."));
        Assertions.assertEquals(HttpStatus.NOT_FOUND, r.getStatusCode());
    }

    @Test
    public void deveReporQuantidadeDasNotas() {
        notasRepository.deleteAll();

        ResponseEntity<String> r = new TestRestTemplate().exchange(getUrl(), HttpMethod.POST, new HttpEntity<>(null, null), String.class);

        Assertions.assertNotNull(r.getBody());
        Assertions.assertEquals(HttpStatus.CREATED, r.getStatusCode());
        final Optional<List<Nota>> allByOrderByValorDesc = notasRepository.findAllByOrderByValorDesc();
        Assertions.assertTrue(allByOrderByValorDesc.isPresent());
        Assertions.assertEquals(4, allByOrderByValorDesc.get().size());
    }

    private String getUrl() {
        return "http://localhost:" + port + "/notas";
    }
}
