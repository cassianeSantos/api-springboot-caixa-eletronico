package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.*;

@SpringBootTest
class NotaServiceTest {

    @Captor
    ArgumentCaptor<Nota> notasCaptor;

    @Mock
    private NotasRepository notasRepository;

    @InjectMocks
    private NotasService notasService;

    @BeforeEach
    public void beforeAll() {
        ReflectionTestUtils.setField(notasService, "quantidadeReposicao", 10);
    }

    @DisplayName("Dado que não tenha nenhuma NotaDto disponível" +
            "quando repor as quantidades" +
            "deve haver todas as NotaDtos naquela quantidade")
    @Test
    public void reporQuantidadeQuandoZerada() {
        // given
        Mockito.when(notasRepository.findByValor(anyDouble())).thenReturn(Optional.empty());

        // when
        notasService.reporQuantidadeNotas();

        // then
        verify(notasRepository, times(4)).save(any(Nota.class));
    }

    @DisplayName("Dado que tenha algumas NotaDtos disponiveis" +
            "quando repor as quantidades" +
            "deve incrementar a quantidade")
    @Test
    public void reporQuantidadeQuandoExistente() {
        // given
        final Nota nota = getNota(1, 10.0, 1L);
        Mockito.when(notasRepository.findByValor(nota.getValor())).thenReturn(Optional.of(nota));

        // when
        notasService.reporQuantidadeNotas();

        // then
        nota.setQuantidade(11);
        verify(notasRepository, times(4)).save(notasCaptor.capture());
        final List<Nota> allValues = notasCaptor.getAllValues();
        assertTrue(allValues.contains(nota));
    }

    @DisplayName("Dado que tenha 5 NotaDtos disponiveis" +
            "quando atualizar as quantidades após saque das 5 NotaDtos" +
            "deve remover a NotaDto da lista")
    @Test
    public void atualizarQuantidadeAposSaqueComTodasAsNotaDtos() {
        // given
        final Nota nota = getNota(5, 10.0, 1L);
        final NotaDto notaDto = getNotaDto(5, 10.0);
        Mockito.when(notasRepository.findByValor(nota.getValor())).thenReturn(Optional.of(nota));

        // when
        notasService.atualizarQuantidades(Lists.newArrayList(notaDto));

        // then
        verify(notasRepository, times(1)).delete(nota);
    }

    @DisplayName("Dado que tenha 5 NotaDtos disponiveis" +
            "quando atualizar as quantidades após saque de 3 NotaDtos" +
            "deve atualizar a quantidade para 2")
    @Test
    public void atualizarQuantidadeAposSaque() {
        // given
        final Nota nota = getNota(5, 50.0, 2L);
        final Nota notaEsperada = getNota(2, 50.0, 2L);
        final NotaDto notaSaque = getNotaDto(3, 50.0);
        Mockito.when(notasRepository.findByValor(nota.getValor())).thenReturn(Optional.of(nota));

        // when
        notasService.atualizarQuantidades(Lists.newArrayList(notaSaque));

        // then
        verify(notasRepository, times(1)).save(notaEsperada);
    }

    @DisplayName("Dado que não tenha nenhuma NotaDto disponível" +
            "quando buscar as NotaDtos disponíveis" +
            "deve exibir a mensagem Nenhuma NotaDto disponível e status 404 Not found")
    @Test
    public void dadoQueNaoTenhaNenhumaNotaDtoDisponivelDeveLancarExcecao() {
        // given
        when(notasRepository.findAllByOrderByValorDesc()).thenReturn(Optional.empty());

        // when
        NenhumaNotaDisponivelException thrown = assertThrows(
                NenhumaNotaDisponivelException.class,
                () -> notasService.getNotasDisponiveis(),
                "Expected notasService.getNotasDisponiveis() to throw NenhumaNotaDisponivelException, mas não ocorreu"
        );

        // then
        assertNotNull(thrown);
        assertTrue(thrown.getMessage().contains("Nenhuma nota disponível."));
    }

    private Nota getNota(int quantidade, double valor, Long id) {
        return Nota.builder().id(id).quantidade(quantidade).valor(valor).build();
    }

    private NotaDto getNotaDto(int quantidade, double valor) {
        return NotaDto.builder().quantidade(quantidade).valor(valor).build();
    }
}