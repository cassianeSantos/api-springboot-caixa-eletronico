package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NotasRepository extends CrudRepository<Nota, Long> {

    Optional<Nota> findByValor(Double valor);

    Optional<List<Nota>> findAllByOrderByValorDesc();
}
