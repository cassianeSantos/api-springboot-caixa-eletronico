package br.com.sistemabancario.caixaeletronico.notas.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@NoArgsConstructor
@Data
@Entity
public class Nota {

    @Id
    @GeneratedValue
    private Long id;

    private Double valor;

    private Integer quantidade;

    @Builder
    public Nota(Long id, Double valor, Integer quantidade) {
        this.id = id;
        this.valor = valor;
        this.quantidade = quantidade;
    }
}
