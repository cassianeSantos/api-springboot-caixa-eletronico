package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class NotasService {

    @Value("${reposicao.notas.quantidade}")
    private Integer quantidadeReposicao;

    private NotasRepository notasRepository;

    private NotasConverter notasConverter;

    @Autowired
    public NotasService(NotasRepository notasRepository, NotasConverter notasConverter) {
        this.notasRepository = notasRepository;
        this.notasConverter = notasConverter;
    }

    public List<NotaDto> getNotasDisponiveis() throws NenhumaNotaDisponivelException {
        log.info("Realizando a busca das notas disponíveis.");
        return notasRepository.findAllByOrderByValorDesc()
                .orElseThrow(NenhumaNotaDisponivelException::new)
                .stream().map(n -> notasConverter.convert(n))
                .collect(Collectors.toList());
    }

    @Scheduled(cron = "${reposicao.notas.periodo}")
    public void reporQuantidadeNotas() {
        final Nota notaDez = Nota.builder().valor(10.0).quantidade(quantidadeReposicao).build();
        final Nota notaVinte = Nota.builder().valor(20.0).quantidade(quantidadeReposicao).build();
        final Nota notaCinquenta = Nota.builder().valor(50.0).quantidade(quantidadeReposicao).build();
        final Nota notaCem = Nota.builder().valor(100.0).quantidade(quantidadeReposicao).build();
        List<Nota> notaReposicao = Lists.newArrayList(notaDez, notaVinte, notaCinquenta, notaCem);

        notaReposicao.forEach(nota -> notasRepository.findByValor(nota.getValor())
                .ifPresentOrElse((value) -> {
                            log.debug("Nota encontrada, atualizando quantidade. {}", nota.getValor());
                            atualizarQuantidade(value, nota.getQuantidade(), true);
                        },
                        () -> {
                            log.debug("Nota em falta. {}", nota.getValor());
                            notasRepository.save(nota);
                        }));
        log.info("Quantidade atualizadas com sucesso.");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void atualizarQuantidades(final List<NotaDto> notaDoSaque) {
        notaDoSaque.forEach(nota -> notasRepository.findByValor(nota.getValor()).ifPresent(
                notaSalva -> atualizarQuantidade(notaSalva, nota.getQuantidade(), false)
            ));

        log.info("Quantidades atualizadas com sucesso.");
    }

    private void atualizarQuantidade(final Nota notaSalva, final Integer quantidadeSaque, final boolean incrementar) {
        final int quantidade = incrementar ? notaSalva.getQuantidade() + quantidadeSaque : notaSalva.getQuantidade() - quantidadeSaque;
        log.debug("Quantidade atual: {}", notaSalva.getQuantidade());

        if (quantidade == 0) {
            log.debug("Quantidade zerada, removendo a nota: {}", notaSalva.getValor());
            notasRepository.delete(notaSalva);
        } else {
            notaSalva.setQuantidade(quantidade);
            log.debug("Quantidade atualizada para: {}", quantidade);
            notasRepository.save(notaSalva);
        }
    }
}
