package br.com.sistemabancario.caixaeletronico.notas.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class NotaDto {

    private Double valor;
    private Integer quantidade;

    @Builder
    public NotaDto(Double valor, Integer quantidade) {
        this.valor = valor;
        this.quantidade = quantidade;
    }
}
