package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.notas.model.Nota;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class NotasConverter implements Converter<Nota, NotaDto> {

    @Override
    public NotaDto convert(Nota source) {
        return NotaDto.builder()
                .quantidade(source.getQuantidade())
                .valor(source.getValor())
                .build();
    }
}
