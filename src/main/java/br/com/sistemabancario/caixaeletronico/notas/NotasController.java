package br.com.sistemabancario.caixaeletronico.notas;

import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Controller
@RequestMapping(path = "/notas", produces = {"application/json"})
public class NotasController {

    private NotasService notasService;

    @Autowired
    public NotasController(NotasService notasService) {
        this.notasService = notasService;
    }

    @GetMapping
    public ResponseEntity<List<NotaDto>> getNotasDisponiveis() throws NenhumaNotaDisponivelException {
        return ResponseEntity.ok(notasService.getNotasDisponiveis());
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED, reason = "reposição efetuada com sucesso.")
    public void reporQuantidadeDeNotas() {
        notasService.reporQuantidadeNotas();
    }
}
