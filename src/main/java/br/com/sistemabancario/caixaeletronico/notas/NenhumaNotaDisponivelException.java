package br.com.sistemabancario.caixaeletronico.notas;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Nenhuma nota disponível.")
public class NenhumaNotaDisponivelException extends Exception {

    public NenhumaNotaDisponivelException() {
        super("Nenhuma nota disponível.");
    }
}
