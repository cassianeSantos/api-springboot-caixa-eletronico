package br.com.sistemabancario.caixaeletronico.saque;

import br.com.sistemabancario.caixaeletronico.notas.NenhumaNotaDisponivelException;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(path = "/saque")
public class SaqueController {

    private SaqueService saqueService;

    @Autowired
    public SaqueController(SaqueService saqueService) {
        this.saqueService = saqueService;
    }

    @GetMapping(path = "/{valor}")
    public ResponseEntity<List<NotaDto>> realizarSaque(@PathVariable Double valor) throws BusinessException, NenhumaNotaDisponivelException {
        return ResponseEntity.ok(saqueService.realizar(valor));
    }
}
