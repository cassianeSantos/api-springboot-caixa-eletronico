package br.com.sistemabancario.caixaeletronico.saque;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BusinessException extends Exception {

    public BusinessException(String message) {
        super(message);
    }
}
