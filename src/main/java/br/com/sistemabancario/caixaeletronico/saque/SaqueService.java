package br.com.sistemabancario.caixaeletronico.saque;

import br.com.sistemabancario.caixaeletronico.notas.NenhumaNotaDisponivelException;
import br.com.sistemabancario.caixaeletronico.notas.NotasService;
import br.com.sistemabancario.caixaeletronico.notas.model.NotaDto;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class SaqueService {

    private NotasService notasService;

    @Autowired
    public SaqueService(NotasService notasService) {
        this.notasService = notasService;
    }

    public List<NotaDto> realizar(final Double valor) throws NenhumaNotaDisponivelException, BusinessException {
        log.info("Realizando saque de valor: {}", valor);
        isValorMultiploDeDoisEMaiorQueDez(valor);

        final List<NotaDto> notasDisponiveis = notasService.getNotasDisponiveis();
        isValorAbaixoDoDisponivel(valor, notasDisponiveis);

        final List<NotaDto> notaDoSaque = getNotasDoSaque(0, notasDisponiveis, valor, Lists.newArrayList());
        log.info("Notas do saque: {}", notaDoSaque);

        notasService.atualizarQuantidades(notaDoSaque);

        return notaDoSaque;
    }

    private void isValorMultiploDeDoisEMaiorQueDez(Double valor) throws ValorNaoEMultiploDeDez {
        if (!(valor % 10 == 0) || valor < 10) {
            log.error("Valor não é múltiplo de dez.");
            throw new ValorNaoEMultiploDeDez();
        }
    }

    private void isValorAbaixoDoDisponivel(final Double valor, final List<NotaDto> notaDisponiveis) throws ValorDeSaqueAcimaDoDisponivel, ValorDeSaqueAcimaDoPermitido {
        if (valor > notaDisponiveis.stream().mapToDouble(n -> n.getValor() * n.getQuantidade()).sum()) {
            log.error("Valor de saque acima do disponível.");
            throw new ValorDeSaqueAcimaDoDisponivel();
        } else if (valor > 2000.0) {
            log.error("Valor de saque acima do permitido.");
            throw new ValorDeSaqueAcimaDoPermitido();
        }
    }

    public List<NotaDto> getNotasDoSaque(int pos, List<NotaDto> notaList, Double valor, List<NotaDto> notaDoSaque) {
        if (valor == 0) {
            return notaDoSaque;
        }

        int notasInteiras = Double.valueOf(valor / notaList.get(pos).getValor()).intValue();

        if (notasInteiras > 0) {
            if (notasInteiras > notaList.get(pos).getQuantidade()) {
                valor -= (notaList.get(pos).getQuantidade() * notaList.get(pos).getValor());
            } else {
                valor -= (notasInteiras * notaList.get(pos).getValor());
                notaList.get(pos).setQuantidade(notasInteiras);
            }

            notaDoSaque.add(notaList.get(pos));
        }

        return getNotasDoSaque(pos + 1, notaList, valor, notaDoSaque);
    }
}
