package br.com.sistemabancario.caixaeletronico.saque;

public class ValorDeSaqueAcimaDoPermitido extends BusinessException {

    public ValorDeSaqueAcimaDoPermitido() {
        super("Valor de saque acima do permitido.");
    }
}
