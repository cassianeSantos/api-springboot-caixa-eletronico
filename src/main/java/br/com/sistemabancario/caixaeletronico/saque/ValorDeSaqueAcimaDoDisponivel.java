package br.com.sistemabancario.caixaeletronico.saque;

public class ValorDeSaqueAcimaDoDisponivel extends BusinessException {

    public ValorDeSaqueAcimaDoDisponivel() {
        super("Valor acima do disponível.");
    }
}
