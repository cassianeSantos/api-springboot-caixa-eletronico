package br.com.sistemabancario.caixaeletronico.saque;

public class ValorNaoEMultiploDeDez extends BusinessException {

    public ValorNaoEMultiploDeDez() {
        super("Valor deve ser múltiplo de dez.");
    }
}
