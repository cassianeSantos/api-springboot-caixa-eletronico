# Read Me First
Esta api é parte do sistema bancário que venho criando.
Para uma maior complexidade foi escolhido ter o controle de quantidade de cada nota disponível no caixa eletronico. 

Importe o projeto na ide de preferencia, particularmente estava utilizando Intellij. 

Este projeto utiliza: 
 - Gradle para as dependencias.
 - Java 11
 - Spring Boot, Spring Data, Spring Web e Devtools
 - Banco em memória H2
 - Spring Test com JUnit 5 (Testes de integração e unitários). 

# Getting Started #

 - Sempre que rodar a task do gradle bootRun deve seguir esta ordem:
    - curl --location --request GET 'localhost:8080/notas'
    Para verficair as notas que estão disponíveis.
    
    -  curl --location --request POST 'localhost:8080/notas'
    Para repor as notas com a quantidade parametrizada no arquivo (Atual 10).
    
    - curl --location --request GET 'localhost:8080/saque/200.0'
    Para realizar a operação de saque. 

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/gradle-plugin/reference/html/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/htmlsingle/#configuration-metadata-annotation-processor)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.2.6.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

